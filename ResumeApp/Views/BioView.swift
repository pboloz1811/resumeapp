//
//  BioView.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 28/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import UIKit

class BioView: UIView {

    struct Sizes {
        let imageViewSize = CGSize(width: 100, height: 100)
    }

    struct Constants {
        let topMargin: CGFloat = 10
        let bottomMargin: CGFloat = 10
        let labelHeight: CGFloat = 20
    }

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = sizes.imageViewSize.width / 2
        imageView.clipsToBounds = true
        return imageView
    }()

    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()

    private lazy var professionLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()

    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }()

    private let sizes = Sizes()
    private let constants = Constants()

    init() {
        super.init(frame: CGRect.zero)
        setup()
        layout()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func update(bio: Bio) {
        nameLabel.text = bio.fullName
        professionLabel.text = bio.profession
        imageView.with(imageUrl: bio.imageUrl)
    }

    private func setup() {
        addSubview(imageView)
        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(professionLabel)
        addSubview(stackView)
    }

    private func layout() {
        imageView.layoutView(constraints: [
            imageView.heightAnchor.constraint(equalToConstant: sizes.imageViewSize.height),
            imageView.widthAnchor.constraint(equalToConstant: sizes.imageViewSize.width),
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: constants.topMargin).withPriority(UILayoutPriority(rawValue: 999)),
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])

        stackView.layoutView(constraints: [
            stackView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: constants.bottomMargin),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -constants.bottomMargin).withPriority(UILayoutPriority(rawValue: 999)),
            stackView.centerXAnchor.constraint(equalTo: imageView.centerXAnchor)
        ])

        nameLabel.layoutView(constraints: [
            nameLabel.heightAnchor.constraint(equalToConstant: constants.labelHeight)
        ])

        nameLabel.setContentHuggingPriority(.required, for: .vertical)
        nameLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 999), for: .vertical)


        professionLabel.layoutView(constraints: [
            professionLabel.heightAnchor.constraint(equalToConstant: constants.labelHeight)
        ])

        professionLabel.setContentHuggingPriority(.required, for: .vertical)
        professionLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 998), for: .vertical)
    }
}
