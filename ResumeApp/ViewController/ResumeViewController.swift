//
//  ResumeViewController.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 28/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import UIKit

class ResumeViewController: UIViewController {

    private let viewModel: ResumeViewModel
    private let tableView = UITableView()
    private var tableViewDataSource: TableViewDataSource!

    init(viewModel: ResumeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        tableView.allowsSelection = false
        layout()
        self.tableViewDataSource = TableViewDataSource(viewModel: viewModel.tableViewModel, tableView: tableView)
    }

    private func layout() {
        view.addSubview(tableView)
        tableView.layoutView(constraints: [
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
    }
}

extension ResumeViewController: ResumeViewModelDelegate {

    func updateHeader(title: String) {
        self.title = title
    }

    func operationFailed(error: Error) {
        handleAndPresentError(error: error)
    }
}

