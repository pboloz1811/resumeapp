//
//  ApiClient.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 28/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation


final class ApiClient {

    func executeRequest<Response: Decodable>(url: RestApiUrl, httpMethod: HttpMethod, responseType: Response.Type, completion: @escaping (Result<Response, ApiError>) -> Void) {
        executeRequest(url: url, httpMethod: httpMethod) {
            [unowned self] result in
            DispatchQueue.main.async {
                switch result {
                case .failure(let error):
                    completion(.failure(error))
                case .success(let data):
                    guard let response: Response = try? self.decode(data: data) else {
                        completion(.failure(.parseError))
                        return
                    }
                    completion(.success(response))
                }
            }
        }
    }

    private func executeRequest(url: RestApiUrl, httpMethod: HttpMethod, completion: @escaping (Result<Data, ApiError>) -> Void) {
        guard let request = buildRequest(restApiUrl: url, method: httpMethod) else {
            completion(.failure(.requestError))
            return
        }
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    return completion(.failure(.unacceptableStatusCode))
                }
            }
            guard let data = data else { return completion(.failure(.missingData)) }
            return completion(.success(data))
        }).resume()
    }


    private func buildRequest(restApiUrl: RestApiUrl, method: HttpMethod, httpBody: Data? = nil) -> URLRequest? {
        guard let url = URL(string: restApiUrl.urlAddress) else { return nil }
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.httpBody = httpBody
        return request
    }

    private func decode<Response: Decodable>(data: Data) throws -> Response {
        let decoder = JSONDecoder()
        do {
            let response = try decoder.decode(Response.self, from: data)
            return response
        } catch {
            throw ApiError.parseError
        }
    }

    private func encode<Result: Encodable>(value: Result) throws -> Data {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(value)
            return data
        } catch {
            throw ApiError.encodeError
        }
    }

}

enum HttpMethod: String {
    case get = "GET"
}

enum ApiError: Error {
    case requestError
    case parseError
    case encodeError
    case missingData
    case unacceptableStatusCode

    var localizedDescription: String {
        switch self {
        case .requestError:
            return "Request error"
        case .parseError:
            return "Parse Error"
        case .encodeError:
            return "Encode Error"
        case .missingData:
            return "Missing Data"
        case .unacceptableStatusCode:
            return "Unacceptable Status Code"
        }
    }
}

enum Result<Value, Error: Swift.Error> {
    case success(Value)
    case failure(Error)
}
