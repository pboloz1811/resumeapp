//
//  ResumeClient.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 28/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation


class ResumeClient {

    private let apiClient: ApiClient

    init(apiClient: ApiClient) {
        self.apiClient = apiClient
    }


    func getResume(completion: @escaping (Result<Resume, ApiError>) -> Void) {
        apiClient.executeRequest(url: .getResume,
                                 httpMethod: .get,
                                 responseType: Resume.self,
                                 completion: completion)
    }

}
