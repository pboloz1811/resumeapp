//
//  RestApiUrl.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 28/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation

enum RestApiUrl {

    var urlAddress: String {
        switch self {
        case .getResume:
            return "https://api.myjson.com/bins/xvhtd"
        }
    }

    case getResume
}
