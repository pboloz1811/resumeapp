//
//  EducationTableViewCell.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 29/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import UIKit

class EducationTableViewCell: UITableViewCell {

    private struct Constants {
        let margin: CGFloat = 10
    }

    private lazy var universityNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()

    private lazy var universityFieldLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()

    private lazy var universityTimestampLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()

    private lazy var universityDegreeLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()

    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        return stackView
    }()

    private let constants = Constants()

    convenience init()
    {
        self.init(style: .default, reuseIdentifier: nil)
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
        layout()
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
        layout()
    }

    func update(viewModel: EducationCellViewModel) {
        universityNameLabel.text = viewModel.universityName
        universityFieldLabel.text = viewModel.universityField
        universityDegreeLabel.text = viewModel.universityDegree
        universityTimestampLabel.text = viewModel.timestamp
    }

    private func setup() {
        containerStackView.addArrangedSubview(universityNameLabel)
        containerStackView.addArrangedSubview(universityFieldLabel)
        containerStackView.addArrangedSubview(universityDegreeLabel)
        containerStackView.addArrangedSubview(universityTimestampLabel)
        contentView.addSubview(containerStackView)
    }


    private func layout() {
        containerStackView.layoutView(constraints: [
            containerStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: constants.margin),
            containerStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: constants.margin),
            containerStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -constants.margin),
            containerStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -constants.margin)
        ])
    }

}
