//
//  ExperienceTableViewCell.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 29/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {

    private struct Constants {
        let margin: CGFloat = 10
    }

    private lazy var companyNameLabel: UILabel = {
        let label = UILabel()
        return label
    }()

    private lazy var positionLabel: UILabel = {
        let label = UILabel()
        return label
    }()

    private lazy var workingTimestampLabel: UILabel = {
        let label = UILabel()
        return label
    }()

    private lazy var responsibilitiesLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()

    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        return stackView
    }()

    private let constants = Constants()

    convenience init()
    {
        self.init(style: .default, reuseIdentifier: nil)
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
        layout()
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
        layout()
    }

    func update(viewModel: ExperienceCellViewModel) {
        companyNameLabel.text = viewModel.companyName
        positionLabel.text = viewModel.position
        workingTimestampLabel.text = viewModel.timestamp
        responsibilitiesLabel.isHidden = !viewModel.hasResponsibilities
        responsibilitiesLabel.text = viewModel.responsibilitiesText
    }

    private func setup() {
        containerStackView.addArrangedSubview(companyNameLabel)
        containerStackView.addArrangedSubview(positionLabel)
        containerStackView.addArrangedSubview(workingTimestampLabel)
        containerStackView.addArrangedSubview(responsibilitiesLabel)
        contentView.addSubview(containerStackView)
    }

    private func layout() {
        containerStackView.layoutView(constraints: [
            containerStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: constants.margin),
            containerStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: constants.margin),
            containerStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -constants.margin),
            containerStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -constants.margin)
        ])
    }
}
