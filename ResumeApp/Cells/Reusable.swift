//
//  Reusable.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 01/12/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation
import UIKit

protocol Reusable { }

extension Reusable where Self: UITableViewCell {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableViewCell: Reusable { }
