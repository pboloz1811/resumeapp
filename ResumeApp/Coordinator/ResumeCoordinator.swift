//
//  ResumeCoordinator.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 28/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation
import UIKit

class ResumeCoordinator: Coordinator {

    private let navigationController: UINavigationController
    private let dependencyContainer: ResumeServiceDependencyProtocol

    var childCoordinators: [Coordinator] = []

    init(navigationController: UINavigationController,
         dependencyContainer: ResumeServiceDependencyProtocol) {
        self.navigationController = navigationController
        self.dependencyContainer = dependencyContainer
    }

    func start() {
        let resumeService = dependencyContainer.resolveResumeService()
        let viewModel = ResumeViewModel(resumeService: resumeService)
        let viewController = ResumeViewController(viewModel: viewModel)
        viewModel.delegate = viewController
        navigationController.pushViewController(viewController, animated: false)
    }
}
