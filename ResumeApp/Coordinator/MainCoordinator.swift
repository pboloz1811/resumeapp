//
//  MainCoordinator.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 28/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation
import UIKit

class MainCoordinator: Coordinator {

    private let window: UIWindow
    var childCoordinators: [Coordinator] = []

    init(window: UIWindow) {
        self.window = window
    }


    func start() {
        let navigationController = UINavigationController()
        navigationController.navigationBar.prefersLargeTitles = true
        let resumeCoordinator = ResumeCoordinator(navigationController: navigationController, dependencyContainer: DependencyContainer.shared)
        resumeCoordinator.start()
        childCoordinators.append(resumeCoordinator)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}
