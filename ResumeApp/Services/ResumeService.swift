//
//  ResumeService.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 28/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation


protocol ResumeServiceProtocol {
    func getResume(completion: @escaping (Result<Resume, ApiError>) -> Void)
}

class ResumeService: ResumeServiceProtocol {

    private let resumeClient: ResumeClient

    init(resumeClient: ResumeClient) {
        self.resumeClient = resumeClient
    }

    func getResume(completion: @escaping (Result<Resume, ApiError>) -> Void) {
        resumeClient.getResume(completion: completion)
    }
}
