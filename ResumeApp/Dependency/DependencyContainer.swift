//
//  DependencyContainer.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 29/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation

protocol ResumeServiceDependencyProtocol {
     func resolveResumeService() -> ResumeServiceProtocol
}

class DependencyContainer {

    private lazy var apiClient = ApiClient()
    private lazy var resumeClient = ResumeClient(apiClient: apiClient)
    private lazy var resumeService = ResumeService(resumeClient: resumeClient)

    static let shared = DependencyContainer()

    private init() { }
}

extension DependencyContainer: ResumeServiceDependencyProtocol {

    func resolveResumeService() -> ResumeServiceProtocol {
        return resumeService
    }

}
