//
//  ResumeSectionType.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 28/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation

enum ResumeSectionType {

    case education([ResumeCellType])
    case experience([ResumeCellType])

    var sectionTitle: String {
        switch self {
        case .education:
            return "Education"
        case .experience:
            return "Experience"
        }
    }
}

enum ResumeCellType {
    case education(EducationCellViewModel)
    case experience(ExperienceCellViewModel)
}

