//
//  Experience.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 28/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation

struct Experience: Decodable {
    let companyName: String
    let position: String
    let startYear: Int
    let endYear: Int?
    let responsibilities: [String]?

    var hasResponsibilities: Bool {
        return responsibilities != nil
    }

    var timestamp: String {
        guard let endYear = endYear else { return  "\(startYear) - present" }
        return  "\(startYear) - \(endYear)"
    }


    private enum CodingKeys: String, CodingKey {
        case companyName = "company_name"
        case position
        case startYear = "start_year"
        case endYear = "end_year"
        case responsibilities
    }

}
