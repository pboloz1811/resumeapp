//
//  Education.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 28/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation


struct Education: Decodable {

    let universityName: String
    let universityDegree: String
    let universityField: String
    let startYear: Int
    let endYear: Int?
    let isFinished: Bool

    var timestamp: String {
        guard let endYear = endYear else { return "\(startYear) - present" }
        return "\(startYear) - \(endYear)"
    }

    private enum CodingKeys: String, CodingKey {
        case universityName = "university_name"
        case universityDegree = "university_degree"
        case universityField = "university_field"
        case startYear = "start_year"
        case endYear = "end_year"
        case isFinished = "finished"
    }
}
