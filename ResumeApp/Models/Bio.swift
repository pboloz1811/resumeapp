//
//  Bio.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 29/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation

struct Bio {
    let name: String
    let surname: String
    let imageUrl: String
    let profession: String

    var fullName: String {
        return "\(name) \(surname)"
    }
}
