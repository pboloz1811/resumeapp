//
//  Resume.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 28/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation

struct Resume: Decodable {
    
    let name: String
    let surname: String
    let imageUrl: String
    let profession: String
    let education: [Education]
    let experience: [Experience]

    private enum CodingKeys: String, CodingKey {
        case name
        case surname
        case profession
        case imageUrl = "image_url"
        case education
        case experience
    }
    
}
