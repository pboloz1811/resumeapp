//
//  UIViewController+Ext.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 29/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func handleAndPresentError(error: Error) {
        let alertController = UIAlertController(title: "Error!", message: error.localizedDescription, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }
}
