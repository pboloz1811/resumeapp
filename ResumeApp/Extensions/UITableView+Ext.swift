//
//  UITableView+Ext.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 28/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import UIKit

extension UITableView {

    func setAndLayoutTableHeaderView(_ header: UIView) {
        self.tableHeaderView = header

        header.setNeedsLayout()
        header.layoutIfNeeded()

        let height = header.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height

        var frame = header.frame
        frame.size.height = height
        header.frame = frame

        self.tableHeaderView = header
    }
}
