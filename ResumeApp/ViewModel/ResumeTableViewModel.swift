//
//  ResumeTableViewModel.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 28/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation

class ResumeTableViewModel {

    enum Action {
        case reload
    }

    var action: ((Action) -> Void)?

    private var sectionTypes: [ResumeSectionType] = []
    var bio: Bio?

    func numberOfSections() -> Int {
        return sectionTypes.count
    }

    func numberOfRowsInSection(section: Int) -> Int {
        switch sectionTypes[section] {
        case .education(let education):
            return education.count
        case .experience(let experience):
            return experience.count
        }
    }

    func cellType(at indexPath: IndexPath) -> ResumeCellType {
        switch sectionTypes[indexPath.section] {
        case .education(let educationCellTypes):
            return educationCellTypes[indexPath.row]
        case .experience(let experienceCellType):
            return experienceCellType[indexPath.row]
        }
    }
    
    func reload(sectionTypes: [ResumeSectionType], bio: Bio) {
        self.sectionTypes = sectionTypes
        self.bio = bio
        action?(.reload)
    }

    func titleForSection(section: Int) -> String {
        return sectionTypes[section].sectionTitle
    }
}
