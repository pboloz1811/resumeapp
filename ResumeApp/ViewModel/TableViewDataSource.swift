//
//  TableViewDataSource.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 28/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation
import UIKit

class TableViewDataSource: NSObject {

    private unowned let tableView: UITableView
    private unowned let viewModel: ResumeTableViewModel

    private let bioView = BioView()

    init(viewModel: ResumeTableViewModel, tableView: UITableView) {
        self.viewModel = viewModel
        self.tableView = tableView
        super.init()
        tableView.dataSource = self
        tableView.setAndLayoutTableHeaderView(bioView)
        tableView.rowHeight = UITableView.automaticDimension
        setupReloadAction()
        registerCells()
    }

    private func registerCells() {
        tableView.register(EducationTableViewCell.self, forCellReuseIdentifier: EducationTableViewCell.reuseIdentifier)
        tableView.register(ExperienceTableViewCell.self, forCellReuseIdentifier: ExperienceTableViewCell.reuseIdentifier)
    }

    private func setupReloadAction() {
        viewModel.action = {
            [weak self] action in
            guard let self = self else { return }
            
            switch action {
            case .reload:
                self.tableView.reloadData()
                guard let bio = self.viewModel.bio else { return }
                self.bioView.update(bio: bio)
            }
        }
    }
}

extension TableViewDataSource: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch viewModel.cellType(at: indexPath) {
        case .education(let viewModel):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: EducationTableViewCell.reuseIdentifier) as? EducationTableViewCell else { return UITableViewCell() }
            cell.update(viewModel: viewModel)
            return cell
        case .experience(let viewModel):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ExperienceTableViewCell.reuseIdentifier) as? ExperienceTableViewCell else { return UITableViewCell() }
            cell.update(viewModel: viewModel)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.titleForSection(section: section)
    }
}
