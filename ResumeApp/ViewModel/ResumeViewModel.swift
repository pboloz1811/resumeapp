//
//  ResumeViewModel.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 28/09/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation

protocol ResumeViewModelDelegate: class {
    func updateHeader(title: String)
    func operationFailed(error: Error)
}

class ResumeViewModel {

    private let resumeService: ResumeServiceProtocol

    weak var delegate: ResumeViewModelDelegate?

    let tableViewModel = ResumeTableViewModel()

    init(resumeService: ResumeServiceProtocol) {
        self.resumeService = resumeService
        getResume()
    }

    private func getResume() {
        resumeService.getResume {
            [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                self.delegate?.operationFailed(error: error)
            case .success(let resume):
                let educationCellTypes = resume.education.map { ResumeCellType.education(EducationCellViewModel(education: $0)) }
                let experienceCellTypes = resume.experience.map { ResumeCellType.experience(ExperienceCellViewModel(experience: $0)) }
                let sectionType = [
                    ResumeSectionType.education(educationCellTypes),
                    ResumeSectionType.experience(experienceCellTypes)
                ]
                let title = "\(resume.name) \(resume.surname)"
                let bio = Bio(name: resume.name, surname: resume.surname, imageUrl: resume.imageUrl, profession: resume.profession)
                self.delegate?.updateHeader(title: title)
                self.tableViewModel.reload(sectionTypes: sectionType, bio: bio)
            }
        }
    }
}
