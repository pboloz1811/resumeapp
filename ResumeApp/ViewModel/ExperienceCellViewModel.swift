//
//  ExperienceCellViewModel.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 01/12/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation

struct ExperienceCellViewModel {

    private let experience: Experience

    var companyName: String {
        return experience.companyName
    }

    var position: String {
        return experience.position
    }

    var timestamp: String {
        return experience.timestamp
    }

    var hasResponsibilities: Bool {
        return experience.hasResponsibilities
    }

    var responsibilitiesText: String? {
        return experience.responsibilities?.compactMap { $0 }.joined(separator: ", ")
    }
    

    init(experience: Experience) {
        self.experience = experience
    }
}
