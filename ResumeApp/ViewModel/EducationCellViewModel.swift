//
//  EducationCellViewModel.swift
//  ResumeApp
//
//  Created by Patryk Boloz on 01/12/2019.
//  Copyright © 2019 Patryk Boloz. All rights reserved.
//

import Foundation

struct EducationCellViewModel {

    private let education: Education

    var universityName: String {
        return education.universityName
    }

    var universityField: String {
        return education.universityField
    }

    var universityDegree: String {
        return "Degree: \(education.universityDegree)"
    }

    var timestamp: String {
        return education.timestamp
    }

    init(education: Education) {
        self.education = education
    }
}
